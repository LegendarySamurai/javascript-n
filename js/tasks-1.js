// Условные конструкции и логические операторы

// 1. var x = prompt(‘Введите число’, ‘’) - вывести в alert, четное число или нечетное.
var task1 = function() {
    var x = prompt("Введите число", "");

    // проверка число/не число
    if(!+x)
        alert("Вы ввели не число");
    else
        if(x2n % 2 == 0)
            alert("Вы ввели четное число");
        else
            alert("Вы ввели нечетное число");
};

// task1();



// 2. В переменную x записывается число, введенное пользователем в диалоговое окно.
// Проверить и вывести на экран, x – отрицательное число, положительное или ноль.

var task2 = function() {
    var x = prompt("Введите число", "");

    // проверка число/не число
    var x2n = Number(x);
    if(!x2n)
        alert("Вы ввели не число");
    else
        if(x >= 0)
            alert("Вы ввели положительное число или ноль");
        else
            alert("Вы ввели отрицательное число");
};

// task2();



// 3. Если переменная test равна true, то выведите 'Верно!', иначе выведите 'Неверно!'.
// Проверьте работу скрипта при test, равном true, false.
// Напишите два варианта скрипта - с использованием условного оператора и тернарного.

var task3_1 = function(test) {
    if(test)
        alert("Верно!");
    else
        alert("Неверно!");
};

var task3_2 = function(test) {
    test ? alert("Верно!") : alert("Неверно!");
};

// task3_1();
// task3_2();



// 4. Запишите в переменные x и y значения, возвращаемые функциями prompt.
// Сложите полученные значения.
// Если результат суммирования больше 10, то умножьте его на 10, в противном случае разделите на 10.
// Выведите на экране полученный результат.

var task4 = function() {

    var x = prompt("Введите первое число", "");
    var y = prompt("Введите второе число", "");

    if(x != "" || y != "") {
        var sum = +x + +y;

        if(sum > 10) {
            sum *= 10;
            alert(sum);
        }

        else if(sum <= 10) {
            sum /= 10;
            alert(sum);
        }
    }

    else
        alert("Значения 'x' и/или 'y' отсутствуют");



};

// task4();



// 5. Если переменная х больше нуля и меньше пяти, то выведите 'Верно!',
// иначе выведите 'Неверно!'.
// Проверьте работу скрипта при х, равном 5, 0, -3, 2.

var task5 = function(x) {
    if(x == undefined)
        alert("Значение 'x' отсутствует");

    else if(x > 0 && x < 5)
        alert("Верно!");
    else
        alert("Неверно!");

    // C помощью тернарного оператора:
    // (x > 0 && x < 5) ? alert("Верно!") : alert("Неверно!");
};

// task5(2);



// 6. Если переменная х равна нулю или равна двум, то поделите ее на 10,
// иначе прибавьте к ней 7 и выведите ее на экран.
// Проверьте работу скрипта при х, равном 5, 0, -3, 2.

var task6 = function(x) {
    if(x == undefined)
        alert("Значение 'x' отсутствует");

    else if(x == 0 || x == 2) {
        x /= 10;
        alert(x);
    }

    else {
        x += 7;
        alert(x);
    }
};

// task6(-3);



// 7. Если переменная х не равна 1 или не равна 3, то выведите 'Верно!',
// иначе выведите 'Неверно!'. Проверьте работу скрипта при х, равном 1, 0, 3, 2.

var task7 = function(x) {
    if(x == undefined)
        alert("Значение 'x' отсутствует");

    else if(x == 1 || x == 3)
        alert("Неверно!");

    else
        alert("Верно!");
};

task7(1);



// 8.Если переменная х больше нуля и меньше пяти, то увеличьте х на 1, иначе уменьшите x на 1.
// Выведите новое значение переменной на экран. Проверьте работу скрипта при х, равном 5, 0, -3, 9.


var task8 = function(x) {
    if(x == undefined)
        alert("Значение 'x' отсутствует");

    else if(x > 0 && x < 5) {
        x++;
        alert(x);
    }

    else {
        x--;
        alert(x);
    }

}

// task8(9);



// 9. Если переменная x равна или меньше 1, а переменная y больше или равна 3,
// то выведите сумму этих переменных, иначе выведите 'Неверно!'.
// Проверьте работу скрипта при x и y, равным 1 и 3, 0 и 6, 3 и 5.

var task9 = function(x, y) {
    if(x == undefined || y == undefined)
        alert("Значения 'x' и 'y' отсутствуют");

    else if(x <= 1 && y >= 3) {
        var sum = x + y;
        alert(sum);
    }

    else
        alert("Неверно!");
};

// task9(3, 5);



// 10. Если переменная x больше 2-х и меньше 11-ти,
// или переменная y больше или равна 6-и и меньше 14-ти, то увеличьте x на 2,
// иначе прибавьте к x число 5.
// Выведите новое значение переменной на экран. Проверьте работу скрипта самостоятельно.

var task10 = function(x, y) {
    if(x == undefined || y == undefined)
        alert("Значения 'x' и 'y' отсутствуют");

    else if((x > 2 && x < 11) || (y >= 6 && y < 14)) {
        x += 2;
    }

    else
        x += 5;

    alert(x);
};

// task10(4, 1);



// 11. Создайте две переменные: greeting и lang.
// Переменная greeting – пустая строка.
// Переменная lang может принимать три значения: 'ru', 'en', 'fr'.
// Если она имеет значение 'ru', то в переменную greeting запишите приветствие на русском языке,
// если имеет значение 'en' – то на английском, если 'de' – на немецком.
// Выведите на экран приветствие в зависимости от значения переменной lang.
// Решите задачу через if-else и через switch-case.

var task11_1 = function(l) {
    var greeting = "", lang = l; // все таки создам переменную "lang"

    if(lang == "ru")
        greeting = "Привет";
    else if(lang == "en")
        greeting = "Hello";
    else if(lang == "fr")
        greeting = "Salut";
    else
        greeting = "Indicate one of the following languages: 'ru', 'en', 'fr'";

    alert(greeting);

};

var task11_2 = function(l) {
    var greeting = "", lang = l;

    switch(lang) {
        case "ru":
            greeting = "Привет";
            break;

        case "en":
            greeting = "Hello";
            break;

        case "fr":
            greeting = "Salut";
            break;

        default:
            greeting = "Indicate one of the following languages: 'ru', 'en', 'fr'";
            break;
    }

    if(greeting.length > 1)
        alert(greeting);
};

// task11_1();
// task11_2();



// 12. В переменной month лежит число от 1 до 12.
// Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).

var task12 = function(month) {
    var season;

    switch(month) {
        case 1:
        case 2:
        case 12:
            season = "It's winter";
            break;
        case 3:
        case 4:
        case 5:
            season = "It's spring";
            break;
        case 6:
        case 7:
        case 8:
            season = "It's summer";
            break;
        case 9:
        case 10:
        case 11:
            season = "It's fall";
            break;
    }

    alert(season);
};

// task12(2);



// 13. Переменная lang может принимать два значения: 'ru' и 'en'.
// Переменная day принимает значение от 1 до 7-ми.
// Если lang имеет значение 'ru', то в переменную result запишите название дня недели
// на русском языке в соответствии со значением переменной day (1 - понедельник, 2 - вторник и т.д.).
// Если же lang имеет значение 'en' – то аналогично, но день недели будет на английском.

var task13 = function(l, d) {
    var lang = l, day = d, result;

    switch(lang) {
        case "ru":
            switch(day) {
                case 1:
                    result = "Понедельник";
                    break;
                case 2:
                    result = "Вторник";
                    break;
                case 3:
                    result = "Среда";
                    break;
                case 4:
                    result = "Четверг";
                    break;
                case 5:
                    result = "Пятница";
                    break;
                case 6:
                    result = "Суббота";
                    break;
                case 7:
                    result = "Воскресенье";
                    break;
            }
            break;

        case "en":
            switch(day) {
                case 1:
                    result = "Monday";
                    break;
                case 2:
                    result = "Tuesday";
                    break;
                case 3:
                    result = "Wednesday";
                    break;
                case 4:
                    result = "Thursday";
                    break;
                case 5:
                    result = "Friday";
                    break;
                case 6:
                    result = "Saturday";
                    break;
                case 7:
                    result = "Sunday";
                    break;
            }
            break;
    }

    alert(result);
};

// task13("en", 6);